R Topics
-----------------
6 Classes Of the following Topics
Basic R
	RStudio Landscape
	Variables, Syntaxes, Classes
	Scripts, headers & Licenses
	For Loops and IF statements
	Basic Functions, which, length, mean, sd, etc.
	Installing packages, library()
Graphics
	Review Basic R
	Basic Graphics, GGplot, pairwise plots - Today's Tea Time Lesson
Data Manipulation 
	NAs, Binding data, Deleting rows, date formats
	Clustering
Linear Regression/Modeling 
	Run through example dataset making various regression models
	Assessing fit and R squared, predicting values, etc.
Other Data Fitting  
	Peak fitting and Hyperspec
Package Creation
	Run through creating all aspects necessary for basic packages


